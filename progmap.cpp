#include "funcoes.h"

int main(void)
{
    map<int, no> agenda;
    map<int, no>::iterator aux;
    string nome;
	int codigo;
	int idade;
	int opcao;
	no auxag;
	
    do
	{
		cout<<"\nPratica Map"<<endl<<endl;
		
        cout << "\n=    (1) - Inserir - com array - se duplicar - substitui o anterior =";
        cout << "\n=    (2) - Mostrar todos                               =";
        cout << "\n=    (3) - Excluir                                     =";
        cout << "\n=    (4) - Buscar pelo campo chave c/ find             =";
        cout << "\n=    (5) - Buscar pelo campo chave - com array         =";
        cout << "\n=    (6) - Buscar pelo campo nao chave                 =";
        cout << "\n=    (7) - Inserir - com make_pair     - nao substitui =";
        cout << "\n=    (8) - Numero de elementos                         =";
        cout << "\n=    (9) - Numero de registros com a chave especificada=";
        cout << "\n=    (0) - Sair                                        ="<<endl<<endl;
		cout<<"Opcao  selecionada: ";
		cin>>opcao;
		
		switch(opcao)
		{
			case 1: cout<<"\nCod: ";
                    cin>>codigo;
                    cout<<"\nNome: ";
                    cin>>nome;
                    cout<<"\nIdade: ";
                    cin>>idade;

                    auxag.setnome(nome);
                    auxag.setid(idade);
                    
                    agenda[codigo] = auxag;
                    cout<<"\n\nRegistro inserido.\n\n";
         	break;

			case 2: if(!agenda.empty())
                    {
                        for(aux = agenda.begin(); aux != agenda.end(); aux++)
                        {
                            cout<<"Codigo: "<<aux->first;
                            aux->second.lista();
                        }
                    }
                    else
                    {
                        cout<<"\nLista vazia.\n";
                    }                              
			break;
			
			case 3: cout<<"\nInforme o codigo a ser excluido: ";
                    cin>>codigo;
                    
                    aux = agenda.find(codigo);
                    
                    if(aux != agenda.end())
                    {
                        agenda.erase(codigo);
                        cout<<"\n\nRegistro excluido.\n\n";
                    }
                    else
                    {
                        cout<<"\nCodigo nao existe.\n";
                    }
            break;
			
			case 4: cout<<"\nInforme o codigo a ser pesquisado: ";
                    cin>>codigo;
                    
                    aux = agenda.find(codigo);
                    
                    if(aux != agenda.end())
                    {
                        aux->second.lista();
                    }
                    else
                    {
                        cout<<"\nCodigo nao existe.\n";
                    }
			break;
			
			case 5: cout<<"\nInforme o codigo a ser pesquisado: ";
                    cin>>codigo;
                    
                    aux = agenda.find(codigo);
                    
                    if(aux != agenda.end())
                    {
                        agenda[codigo].lista();
                    }
                    else
                    {
                        cout<<"\nCodigo nao existe.\n";
                    }
			break;

			case 6: {
                    cout<<"\nInforme o nome a ser pesquisado: ";
                    cin>>nome;
                    
                    bool achou = false;
                    
                    for(aux = agenda.begin(); aux != agenda.end(); aux++)
                    {
                        if(nome == aux->second.getnome())
                        {
                            achou = true;
                            cout<<"\nCodigo: "<<aux->first;
                            cout<<"\nIdade: "<<aux->second.getid();
                        }
                    }
                    if(achou == false)
                    {
                        cout<<"\nNome nao existe.\n";
                    }
                    }
			break;	
			
			case 7: cout<<"\nCod: ";
                    cin>>codigo;
                    cout<<"\nNome: ";
                    cin>>nome;
                    cout<<"\nIdade: ";
                    cin>>idade;

                    auxag.setnome(nome);
                    auxag.setid(idade);
                    
                    aux = agenda.find(codigo);
                    
                    if(aux == agenda.end())
                    {
                        agenda.insert(make_pair(codigo, auxag));
                        cout<<"\n\nRegistro inserido.\n\n";
                    }
                    else
                    {
                        cout<<"\nCodigo ja existe.\n";
                    }        
			break;
   	
   			case 8: cout<<"\nNumero de elementos: "<<agenda.size();
			break;	

   			case 9: cout<<"\nInforme o codigo a ser pesquisado o numero de repeticoes: ";
                    cin>>codigo;

                    cout<<"\nNumero de repeticoes: "<<agenda.count(codigo);
			break;	   		
			
			default: cout<<"opcao invalida";
			system("cls");

		}
		cout<<endl;
		system("pause");
		system("cls");
} while(opcao != 0);

return(1);

}
